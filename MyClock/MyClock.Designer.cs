﻿namespace Demo.Paint
{
    partial class MyClock
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.myContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Lines = new System.Windows.Forms.ToolStripMenuItem();
            this.LineSec = new System.Windows.Forms.ToolStripMenuItem();
            this.ColorSec = new System.Windows.Forms.ToolStripMenuItem();
            this.RedSec = new System.Windows.Forms.ToolStripMenuItem();
            this.GreenSec = new System.Windows.Forms.ToolStripMenuItem();
            this.BlueSec = new System.Windows.Forms.ToolStripMenuItem();
            this.BrownSec = new System.Windows.Forms.ToolStripMenuItem();
            this.BlackSec = new System.Windows.Forms.ToolStripMenuItem();
            this.lineMin = new System.Windows.Forms.ToolStripMenuItem();
            this.ColorMin = new System.Windows.Forms.ToolStripMenuItem();
            this.RedMin = new System.Windows.Forms.ToolStripMenuItem();
            this.GreenMin = new System.Windows.Forms.ToolStripMenuItem();
            this.BlueMin = new System.Windows.Forms.ToolStripMenuItem();
            this.BrownMin = new System.Windows.Forms.ToolStripMenuItem();
            this.BlackMin = new System.Windows.Forms.ToolStripMenuItem();
            this.lineHour = new System.Windows.Forms.ToolStripMenuItem();
            this.ColorHour = new System.Windows.Forms.ToolStripMenuItem();
            this.RedHour = new System.Windows.Forms.ToolStripMenuItem();
            this.GreenHour = new System.Windows.Forms.ToolStripMenuItem();
            this.BlueHour = new System.Windows.Forms.ToolStripMenuItem();
            this.BrownHour = new System.Windows.Forms.ToolStripMenuItem();
            this.BlackHour = new System.Windows.Forms.ToolStripMenuItem();
            this.myTimeLine = new System.Windows.Forms.TrackBar();
            this.time = new System.Windows.Forms.Label();
            this.myContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myTimeLine)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // myContextMenu
            // 
            this.myContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.myContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Lines});
            this.myContextMenu.Name = "myContextMenu";
            this.myContextMenu.Size = new System.Drawing.Size(135, 28);
            // 
            // Lines
            // 
            this.Lines.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LineSec,
            this.lineMin,
            this.lineHour});
            this.Lines.Name = "Lines";
            this.Lines.Size = new System.Drawing.Size(134, 24);
            this.Lines.Text = "Стрелки";
            // 
            // LineSec
            // 
            this.LineSec.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ColorSec});
            this.LineSec.Name = "LineSec";
            this.LineSec.Size = new System.Drawing.Size(157, 26);
            this.LineSec.Text = "Секундная";
            // 
            // ColorSec
            // 
            this.ColorSec.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RedSec,
            this.GreenSec,
            this.BlueSec,
            this.BrownSec,
            this.BlackSec});
            this.ColorSec.Name = "ColorSec";
            this.ColorSec.Size = new System.Drawing.Size(117, 26);
            this.ColorSec.Text = "Цвет";
            // 
            // RedSec
            // 
            this.RedSec.Name = "RedSec";
            this.RedSec.Size = new System.Drawing.Size(173, 26);
            this.RedSec.Text = "Красный";
            this.RedSec.Click += new System.EventHandler(this.RedSec_Click);
            // 
            // GreenSec
            // 
            this.GreenSec.Name = "GreenSec";
            this.GreenSec.Size = new System.Drawing.Size(173, 26);
            this.GreenSec.Text = "Зеленый";
            this.GreenSec.Click += new System.EventHandler(this.GreenSec_Click);
            // 
            // BlueSec
            // 
            this.BlueSec.Name = "BlueSec";
            this.BlueSec.Size = new System.Drawing.Size(173, 26);
            this.BlueSec.Text = "Синий";
            this.BlueSec.Click += new System.EventHandler(this.BlueSec_Click);
            // 
            // BrownSec
            // 
            this.BrownSec.Name = "BrownSec";
            this.BrownSec.Size = new System.Drawing.Size(173, 26);
            this.BrownSec.Text = "Коричневый";
            this.BrownSec.Click += new System.EventHandler(this.BrownSec_Click);
            // 
            // BlackSec
            // 
            this.BlackSec.Name = "BlackSec";
            this.BlackSec.Size = new System.Drawing.Size(173, 26);
            this.BlackSec.Text = "Черный";
            this.BlackSec.Click += new System.EventHandler(this.BlackSec_Click);
            // 
            // lineMin
            // 
            this.lineMin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ColorMin});
            this.lineMin.Name = "lineMin";
            this.lineMin.Size = new System.Drawing.Size(157, 26);
            this.lineMin.Text = "Минутная";
            // 
            // ColorMin
            // 
            this.ColorMin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RedMin,
            this.GreenMin,
            this.BlueMin,
            this.BrownMin,
            this.BlackMin});
            this.ColorMin.Name = "ColorMin";
            this.ColorMin.Size = new System.Drawing.Size(117, 26);
            this.ColorMin.Text = "Цвет";
            // 
            // RedMin
            // 
            this.RedMin.Name = "RedMin";
            this.RedMin.Size = new System.Drawing.Size(173, 26);
            this.RedMin.Text = "Крассный";
            this.RedMin.Click += new System.EventHandler(this.RedMin_Click);
            // 
            // GreenMin
            // 
            this.GreenMin.Name = "GreenMin";
            this.GreenMin.Size = new System.Drawing.Size(173, 26);
            this.GreenMin.Text = "Зеленый";
            this.GreenMin.Click += new System.EventHandler(this.GreenMin_Click);
            // 
            // BlueMin
            // 
            this.BlueMin.Name = "BlueMin";
            this.BlueMin.Size = new System.Drawing.Size(173, 26);
            this.BlueMin.Text = "Синий";
            this.BlueMin.Click += new System.EventHandler(this.BlueMin_Click);
            // 
            // BrownMin
            // 
            this.BrownMin.Name = "BrownMin";
            this.BrownMin.Size = new System.Drawing.Size(173, 26);
            this.BrownMin.Text = "Коричневый";
            this.BrownMin.Click += new System.EventHandler(this.BrownMin_Click);
            // 
            // BlackMin
            // 
            this.BlackMin.Name = "BlackMin";
            this.BlackMin.Size = new System.Drawing.Size(173, 26);
            this.BlackMin.Text = "Черный";
            this.BlackMin.Click += new System.EventHandler(this.BlackMin_Click);
            // 
            // lineHour
            // 
            this.lineHour.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ColorHour});
            this.lineHour.Name = "lineHour";
            this.lineHour.Size = new System.Drawing.Size(157, 26);
            this.lineHour.Text = "Часовая";
            // 
            // ColorHour
            // 
            this.ColorHour.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RedHour,
            this.GreenHour,
            this.BlueHour,
            this.BrownHour,
            this.BlackHour});
            this.ColorHour.Name = "ColorHour";
            this.ColorHour.Size = new System.Drawing.Size(117, 26);
            this.ColorHour.Text = "Цвет";
            // 
            // RedHour
            // 
            this.RedHour.Name = "RedHour";
            this.RedHour.Size = new System.Drawing.Size(173, 26);
            this.RedHour.Text = "Красный";
            this.RedHour.Click += new System.EventHandler(this.RedHour_Click);
            // 
            // GreenHour
            // 
            this.GreenHour.Name = "GreenHour";
            this.GreenHour.Size = new System.Drawing.Size(173, 26);
            this.GreenHour.Text = "Зеленый";
            this.GreenHour.Click += new System.EventHandler(this.GreenHour_Click);
            // 
            // BlueHour
            // 
            this.BlueHour.Name = "BlueHour";
            this.BlueHour.Size = new System.Drawing.Size(173, 26);
            this.BlueHour.Text = "Синий";
            this.BlueHour.Click += new System.EventHandler(this.BlueHour_Click);
            // 
            // BrownHour
            // 
            this.BrownHour.Name = "BrownHour";
            this.BrownHour.Size = new System.Drawing.Size(173, 26);
            this.BrownHour.Text = "Коричневый";
            this.BrownHour.Click += new System.EventHandler(this.BrownHour_Click);
            // 
            // BlackHour
            // 
            this.BlackHour.Name = "BlackHour";
            this.BlackHour.Size = new System.Drawing.Size(173, 26);
            this.BlackHour.Text = "Черный";
            this.BlackHour.Click += new System.EventHandler(this.BlackHour_Click);
            // 
            // myTimeLine
            // 
            this.myTimeLine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.myTimeLine.Location = new System.Drawing.Point(13, 327);
            this.myTimeLine.Maximum = 12;
            this.myTimeLine.Minimum = -12;
            this.myTimeLine.Name = "myTimeLine";
            this.myTimeLine.Size = new System.Drawing.Size(374, 46);
            this.myTimeLine.TabIndex = 1;
            this.myTimeLine.Scroll += new System.EventHandler(this.myTimeLine_Scroll);
            // 
            // time
            // 
            this.time.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.time.AutoSize = true;
            this.time.Location = new System.Drawing.Point(192, 376);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(24, 17);
            this.time.TabIndex = 2;
            this.time.Text = "12";
            // 
            // MyClock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.myContextMenu;
            this.Controls.Add(this.time);
            this.Controls.Add(this.myTimeLine);
            this.DoubleBuffered = true;
            this.Name = "MyClock";
            this.Size = new System.Drawing.Size(400, 414);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MyClock_Paint);
            this.Resize += new System.EventHandler(this.MyClock_Resize);
            this.myContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myTimeLine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ContextMenuStrip myContextMenu;
        private System.Windows.Forms.ToolStripMenuItem Lines;
        private System.Windows.Forms.ToolStripMenuItem LineSec;
        private System.Windows.Forms.ToolStripMenuItem ColorSec;
        private System.Windows.Forms.ToolStripMenuItem RedSec;
        private System.Windows.Forms.ToolStripMenuItem GreenSec;
        private System.Windows.Forms.ToolStripMenuItem BlueSec;
        private System.Windows.Forms.ToolStripMenuItem BrownSec;
        private System.Windows.Forms.ToolStripMenuItem BlackSec;
        private System.Windows.Forms.ToolStripMenuItem lineMin;
        private System.Windows.Forms.ToolStripMenuItem ColorMin;
        private System.Windows.Forms.ToolStripMenuItem RedMin;
        private System.Windows.Forms.ToolStripMenuItem GreenMin;
        private System.Windows.Forms.ToolStripMenuItem BlueMin;
        private System.Windows.Forms.ToolStripMenuItem BrownMin;
        private System.Windows.Forms.ToolStripMenuItem BlackMin;
        private System.Windows.Forms.ToolStripMenuItem lineHour;
        private System.Windows.Forms.ToolStripMenuItem ColorHour;
        private System.Windows.Forms.ToolStripMenuItem RedHour;
        private System.Windows.Forms.ToolStripMenuItem GreenHour;
        private System.Windows.Forms.ToolStripMenuItem BlueHour;
        private System.Windows.Forms.ToolStripMenuItem BrownHour;
        private System.Windows.Forms.ToolStripMenuItem BlackHour;
        private System.Windows.Forms.TrackBar myTimeLine;
        private System.Windows.Forms.Label time;
    }
}
