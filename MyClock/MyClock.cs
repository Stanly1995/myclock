﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo.Paint
{
    public partial class MyClock : UserControl
    {
        private float[] _sin = new float[360];
        private float[] _cos = new float[360];
        
        Pen s = new Pen(System.Drawing.Color.Red, 3);
        Pen m = new Pen(System.Drawing.Color.Green, 3);
        Pen h = new Pen(System.Drawing.Color.Blue, 4);
        public MyClock()
        {
            
            InitializeComponent();
            myTimeLine.Value = (int)DateTime.Now.Kind;
            for (int i = 0; i < 360; ++i)
            {
                _sin[i] = (float)Math.Sin(i * Math.PI / 180.0F);
                _cos[i] = (float)Math.Cos(i * Math.PI / 180.0F);
            }
        }

        private void MyClock_Paint(object sender, PaintEventArgs e)
        {

            myTimeLine.Top = ClientSize.Height * 5 / 6;
            myTimeLine.Left = ClientSize.Width / 10;
            myTimeLine.Width = ClientSize.Width * 8 / 10;
            time.Top = ClientSize.Height * 11 / 12;
            time.Left = ClientSize.Width / 2;
            time.Text = myTimeLine.Value.ToString();
            time.ForeColor = Color.Blue;
            int hourLine = DateTime.Now.Hour+int.Parse(time.Text)- (int)DateTime.Now.Kind;
            Graphics gr = e.Graphics;

            if (int.Parse(time.Text)<= (int)DateTime.Now.Kind - 12)
            {
                hourLine += 12;
            }
            if (int.Parse(time.Text) >= (int)DateTime.Now.Kind + 12)
            {
                hourLine -= 12;
            }

            PointF centerPoint = new PointF(ClientSize.Width / 2.0F, ClientSize.Height / 2.0F - ClientSize.Height / 8.0F);
            float radiusSec = ClientSize.Width > ClientSize.Height ? ClientSize.Height / 3 : ClientSize.Width / 3;
            float radiusMin = ClientSize.Width > ClientSize.Height ? ClientSize.Height / 3.5F : ClientSize.Width / 3.5F;
            float radiusHours = ClientSize.Width > ClientSize.Height ? ClientSize.Height / 4 : ClientSize.Width / 4;

            int angleSec = DateTime.Now.Second * 6 + 270;
            int angleMin = DateTime.Now.Minute * 6 + 270 + (DateTime.Now.Second * 6) / 60;
            int angleHours = hourLine * 30 + 270 + (DateTime.Now.Minute * 6) / 12 + (DateTime.Now.Second * 6) / 720;
            

            PointF endPointSec = new PointF(centerPoint.X + radiusSec * _cos[angleSec % 360], centerPoint.Y + radiusSec * _sin[angleSec % 360]);
            PointF endPointMin = new PointF(centerPoint.X + radiusMin * _cos[angleMin % 360], centerPoint.Y + radiusMin * _sin[angleMin % 360]);
            PointF endPointHours = new PointF(centerPoint.X + radiusMin * _cos[angleHours % 360], centerPoint.Y + radiusMin * _sin[angleHours % 360]);

            for (int i = 1; i < 13; ++i)
            {
                string dig = i.ToString();
                SizeF sd = gr.MeasureString(dig, this.Font);
                PointF rp = new PointF(centerPoint.X + (radiusSec + 10) * _cos[(i * 30 + 270) % 360] - sd.Width / 2, centerPoint.Y + (radiusSec + 10) * _sin[(i * 30 + 270) % 360] - sd.Height / 2);
                gr.DrawString(dig, this.Font, new SolidBrush(System.Drawing.Color.Blue), rp);
            }

            gr.DrawLine(s, centerPoint, endPointSec);
            gr.DrawLine(m, centerPoint, endPointMin);
            gr.DrawLine(h, centerPoint, endPointHours);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void MyClock_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void RedSec_Click(object sender, EventArgs e)
        {
            s.Color = Color.Red;
        }

        private void GreenSec_Click(object sender, EventArgs e)
        {
            s.Color = Color.Green;
        }

        private void BlueSec_Click(object sender, EventArgs e)
        {
            s.Color = Color.Blue;
        }

        private void BrownSec_Click(object sender, EventArgs e)
        {
            s.Color = Color.Brown;
        }

        private void BlackSec_Click(object sender, EventArgs e)
        {
            s.Color = Color.Black;
        }

        private void RedMin_Click(object sender, EventArgs e)
        {
            m.Color = Color.Red;
        }

        private void GreenMin_Click(object sender, EventArgs e)
        {
            m.Color = Color.Green;
        }

        private void BlueMin_Click(object sender, EventArgs e)
        {
            m.Color = Color.Blue;
        }

        private void BrownMin_Click(object sender, EventArgs e)
        {
            m.Color = Color.Brown;
        }

        private void BlackMin_Click(object sender, EventArgs e)
        {
            m.Color = Color.Black;
        }

        private void RedHour_Click(object sender, EventArgs e)
        {
            h.Color = Color.Red;
        }

        private void GreenHour_Click(object sender, EventArgs e)
        {
            h.Color = Color.Green;
        }

        private void BlueHour_Click(object sender, EventArgs e)
        {
            h.Color = Color.Blue;
        }

        private void BrownHour_Click(object sender, EventArgs e)
        {
            h.Color = Color.Brown;
        }

        private void BlackHour_Click(object sender, EventArgs e)
        {
            h.Color = Color.Black;
        }

        private void myTimeLine_Scroll(object sender, EventArgs e)
        {

        }
    }
}
